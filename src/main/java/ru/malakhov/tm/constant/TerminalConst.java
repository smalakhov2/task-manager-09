package ru.malakhov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String ARG_HELP = "-h";

    String CMD_VERSION = "version";

    String ARG_VERSION = "-v";

    String CMD_ABOUT = "about";

    String ARG_ABOUT = "-a";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String ARG_INFO = "-i";

    String CMD_COMMANDS = "commands";

    String ARG_COMMANDS = "-cmd";

    String CMD_ARGUMENTS = "arguments";

    String ARG_ARGUMENTS = "-arg";

}
